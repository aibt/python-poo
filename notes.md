13 étudiants
salle 61.011


Then download the first notebook " Getting Started " (https://lms.isae.fr/pluginfile.php/65097/mod_label/intro/1-%20Getting%20started.ipynb)
 and save it on your computer (please, create a new directory called "mini project" or "project 1" before...).

From the web page of the notebook server, open this notebook, fill it and save it.


Why a course on Algorithm and Computing?

    because any engineer must be able to use classical computer science tools to manipulate or to create data
    because most of the economic growth at the world-level is due to Information Technology
    because Information Technology is revolutionizing the world (machine learning, big data, Internet of Things, robots, telecoms,...)
    because this programming skills help in everyday life. Please, take few minutes to read these documents on Computational Thinking : the original paper or this post  .
    because there is a huge demand of transverse profiles (including computer science skills) in the aerospace domain !!
    because it's cool  :)

=> any engineer must know how to code !!!!! This is not an option !!

What is a good program ?

    a program which does the job for which it was written ?
    a program which runs fast ?
    a program which is written quickly ?
    a program that can be re-usable by another programmer ?
    another idea ?


During the course, we will use the Python language but this is not a course on Python.

Why Python ?

    Python is one of the most used programming language
    it allows to easily apply the main programming concepts  
    it allows to solve quickly complex problems
    a lot of efficient libraries exist

Take time to visit https://www.python.org/

What if you already know Algorithmic and Computing or Python ?

It is not a problem. The course is built as project-based : for each project of the course, we will focus on a problem which have several levels of resolution : one mandatory part and some optional ones. Of course, to solve this mandatory part, you will need to understand and assimilate some required programming concepts. If you are familiar with Python or with programming, you just will have to go further in the resolution of the problem.
Instructors :

Thibault Gateau ( thibault.gateau$$@$$isae.fr ),  Juan Jesus Torre-Tresols (Juan-jesus.TORRE-TRESOLS$$@$$isae-supaero.fr), Xiaoqi Xu (Xiaoqi.XU$$@$$isae-supaero.fr), Deborah Conforto-Nedelmann  (Deborah.CONFORTO-NEDELMANN$$@$$isae-supaero.fr), Matthias Houssin (Matthias.HOUSSIN$$@$$isae-supaero.fr), Adam Henrique Moreira-Pinto   (adam-henrique.moreira-pinto$$@$$isae-supaero.fr)
.



Marks :

- A programming exam will be organized at the end on the course. Each student will have to write a given program on a computer. This program will be evaluated (2/3 of the global mark).
- Another grade will be obtained by evaluating the uploaded Notebooks (1/3 of the global mark).

And now...

Let's start the first project !


  $v_i = \sqrt{G.M_e \over r} $ 
$\sqrt{\frac{G.Me}{r}}$

