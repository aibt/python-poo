# Introduction

## Course Goal

This class goal is to provide you an introduction to Object Oriented Programming, in Python.


## Course origin 

This module is adapted from Jérôme Lacan's "1MAE002 - ALGORITHM AND COMPUTING" class, itself adapted from large parts from the notebooks proposed in https://github.com/jmportilla/Complete-Python-Bootcamp and https://github.com/fbkarsdorp/python-course . Thank's to the authors.


## Notebook

A notebook is a kind of web page which contains some parts where you can write and execute Python code. In some contexts, the notebooks can be stored and managed on remote servers. In this case, this is the remote server that execute the code.

In this course, for scaling issues, you will have to download the notebook file (with .ipynb extension) and open it in a local notebook server. You will modify, test and save it in local. At the end of the project, you will have to upload it on the LMS.

## Practical organization

Most of this course is on Jupyter notebooks which are interactive sheets which allow you to launch small Python programs illustrating specific concepts.  For each notebook, you have to download the notebook, to fill it, save it and upload it on the LMS.

For larger programs, you will use Spyder, which is a programming environment to quickly develop Python programs. These programs must also be uploaded on the LMS.

All these tools are available on the ISAE computers. However,  you can choose to install these tools on your personal computer and then to bring your own computer during the course to work on it. We recommend this option.


### Personal Computers

If you are on your personal computer, you should launch jupyter notebook server and the python editor spyder from an Anaconda menu.


>The installation is very simple : you just need to install the last version of the distribution of [Anaconda](https://docs.anaconda.com/anaconda/install/index.html).

We will help you if you have problems for the installation. 

### ISAE Commputers

On the ISAE computers,  to launch the local notebook server, first open a terminal. Then, you must load the version of python we will use :

```
module load python/3.7
```

This command allows your system to choose the 3.8 (!) version of Python among the various ones installed on your computer. Then you must load the python our environment :

```
source activate Mae1Fisa1
```

Type the command launching the notebook server :

```
jupyter notebook &   
```

## Additional documentation


In the course, if you have a problem or a question :

- use the Internet. There is a lot of good tutorials on the web :

    - http://www.learnpython.org/

    - http://www.python-course.eu/
    - https://www.codecademy.com/catalog/language/python

    - A very good [python cheat sheet](https://gitlab.isae-supaero.fr/aibt/python-poo/-/raw/master/mementopython3-english.pdf?inline=false)

 - or ask to your colleagues

 - or call your teacher


# 3D visualization of satellite orbits 

![3D visualization of satellite orbits](orbits.png "3D visualization of satellite orbits ")

## 1 - Description of the project

The objective of this project is to represent the circular orbits of satellites around the earth in 3D.
For that, we propose to use the 3D library VPython which allows to represent 3D-objects.  

The project will be organized as follows :

- In a first step, you will analyse and understand a [VPython notebook](https://gitlab.isae-supaero.fr/aibt/python-poo/-/raw/master/BinaryStar.ipynb?inline=false) representing the movements of two planets.

- This analysis will allow you to write your own VPython notebook showing simple satellites in circular orbits around the earth.
 - To simplify the code, we will introduce the concept of classes and objects. This will be done throw an [another notebook](https://gitlab.isae-supaero.fr/aibt/python-poo/-/raw/master/ObjectOrientedProgramming.ipynb?inline=false).
 - Then, your satellite notebook will be rewritten with classes and objects
 - Additionally, an advanced version of satellite with wings and coverage representation will be developed
 - Finally, the optional part of this project will consist in representing a constellation of MEO satellites delivering a message from two random location on the Earth.

From a programming point of view, the main objective of this project is to learn the main concepts of oriented object programming.




## 2 - Working with vPython

[VPython](http://vpython.org/) is a Python library that "makes it easy to create navigable 3D displays and animations". It can take several forms (and several names), but we will use it exclusively in notebooks (no Python programs in this project).

It is already installed on ISAE computers. If you want to use it on your personal computer, you have to install it manually because it is not included in the Anaconda distribution. To install it, try one the following solutions 
type in a terminal  
```
pip install vpython
```

if you are in the ISAE network: 

```
pip install --proxy=http://proxy.isae.fr:3128 vpython" 
```
visit https://conda.anaconda.org/vpython 


## VPython Example

The simplest method to discover VPython is to test a code and to analyze it. Download the [BinaryStar notebook](https://gitlab.isae-supaero.fr/aibt/python-poo/-/raw/master/BinaryStar.ipynb?inline=false) and launch it.
As you can see, the result is quite amazing for a such a simple code ! Could you analyse the code and add your own comments ?

If you need to refresh your knowledge on Newton or Kepler laws, you can have a look at :

 - https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion#Newton.27s_second_law
 - http://www.braeunig.us/space/orbmech.htm#newton ;

 If Jupyter notebook is really not working with VPython, try to use spyder.



## 3 - Your first satellite

>The objective of this section is to launch your first satellite...

 - a) First, create a new notebook. Import the vpython package and create a scene (like in the previous example).

 - b) Create the Earth represented as a sphere.  
The scene has a 3-dimensional cartesian coordinate system where each point is represented by a triplet (x,y,z). Note that each position or vector is a VPython vector Object. You can find a documentation on http://www.vpython.org/contents/docs_vp5/visual/vector.html .

>The Earth must be placed at the center of the scene. It has a radius Re = 6.4e6 (in meters). Its mass is equal to Me = 5.98e24 (in kg).

 - c) The satellite is also represented by a sphere. Of course, we can not choose a radius close to the reality (few meters) because we would not see it... let us choose a radius of 1e6 . In a first step, we will work with MEO (Medium Earth Orbit) satellites like in GPS or Galileo systems. We assume that its altitude is equal to 20e6 meters. Let us position it on the y axis (the vertical one).

>Do not forget that the altitude is the distance to the surface of the Earth. To define the distance between the "sphere", you must add the radius of the Earth.

  - d) Now, we want to put it in movement. According to the Newton laws, to stay in orbit, its initial speed $v_i$ must be equal to: 
  

```math
v_i = \sqrt{G.M_e \over r}
```

  where G=6.67e-11 is the gravitational constant and r is the distance between the satellite and the center of the Earth.The direction of the speed must be orthogonal to the vector from the satellite to the Earth, so let us choose to put it on the x axis (the horizontal one).



Then, just like in the BinaryStar example, you have to implement a loop to update the position of the satellite. For that, you first have to compute the gravitational pull of the Earth. It is represented by a vector oriented toward the center of the Earth and with magnitude equal to:

```math
\frac{G.Me}{r^2}
```


You can use the VPython vector functions norm, mag and/or mag2.
Then you just have to update the speed vector (instead of the momentum p  in the BinaryStar example) and then the position of the satellite.

For the parameters of the loop, take care to the parameters dt and rate which must be chosen carefully. You can take dt=10 and rate(100).

## 4 - Other satellites

When you have implemented a MEO satellite, it is easy for you to implement a LEO and a GEO satellites.
The only difference withe the MEO satellite is their altitude : 163e3 meters for the LEO and 36e6 meters for the GEO.


## 5 - Classes and objects

You should have copied-pasted the python code of the MEO satellite to generate the LEO and MEO ones. It would be quite boring if you should do the same for a whole constellation !
To avoid this, one solution consists in "factorizing" the code. We could create functions that manages the satellite, but we prefer to go one step further by creating a class Satellite which would allows you to generate as many objects satellite with common features but different parameters.

Most of the main concepts of [Classes and objects are presented in this notebook](https://gitlab.isae-supaero.fr/aibt/python-poo/-/raw/master/ObjectOrientedProgramming.ipynb?inline=false). Please, read and fill it.

## 6 - Planet and Satellite classes

>Now, you should be able to rewrite your VPython code by using classes and objects.

a) In a new cell, write a class Planet. This class is defined by \_\_init__ method which has the following parameters (of the \_\_init__ function ): the mass, the radius, the position and the color. In addition, do not forget to add the self parameter.

These parameters are used to initialize the variables of the Planet class:

   - mass
   - position : initial position (a vector)
   - radius
   - s : the sphere which represents the object Planet on the 3D scene 



b) Test your class by representing the Earth and the Moon on a 3D scene. 

The parameters of the Moon are :

   - mass : 0.07346e24 kg
   - radius : 1.736e6 m
   - position of the moon : 378e6 m from the earth  


   c) In a new cell, write a class Satellite. Like the Planet class, this class will have a sphere variable and additional variables characterizing its properties. This class has two methods :

    - __init__ : used to initialize the Satellite. This method has the following parameters :
        * planet : the planet which is the center of the (circular) orbit of the satellite
        * speed : initial speed of the satellite
        * position : defines the initial position of the sphere representing the satellite  
        * size : diameter of the sphere 
        * color : color of the sphere

    -  updatePosition : this method has the parameter dt. It is used to update the speed and the position of the sphere.

Now that you have your class Satellite, represent a LEO, a MEO and a GEO satellites.

>Observe how your code is simple, now !!!

## 6 - Enhanced Satellite

We want to use the fact that the code of the satellite is now factorized in a class to improve it.

a) Improve the aspect of your satellite by adding two "wings" (in reality, solar panels) to the satellite. You can use the VPython objects Box ( http://vpython.org/contents/docs/box.html ).  Don't forget to modify your updatePosition function !

b) Each satellite has a coverage area on the Earth. This area depends on the position of the satellite and on the "angle" of coverage. To represent this area, you can use an Vpython object Cone (http://vpython.org/contents/docs/cone.html) with the origin in the satellite and the circular base on the Earth.
Modify the parameters of the init method and modify the updatePosition function.

For the settings, the coverage angles (the whole cone) of the satellites are :
- LEO : 60°
- MEO : 12°
- GEO : 17°



## 7 - Constellation

Even it is not easy to represent a real constellation, we will try here to represent a small constellation of 12 MEO satellites.

We consider 4 orbits crossing the two poles. Each orbit is used by 3 satellites equally distributed.

Represent this constellation.

Indications :

    - The initial positions of the satellite can be founded by rotating (with the function "rotate") with the correct angle and the correct axis the vector corresponding to the initial position (at the north pole).

    - same remark for the speed.

    - do not consider the problem of collisions at the poles. 


## 8 - Delay-tolerant messaging


a) Now, we consider that the constellations you have implemented are able to store, send and receive "messages".

We consider the following hypothesis :
- a terrestrial antenna (node) on the Earth is able to communicate with a satellite when it is inside the coverage area of the satellite. You can use the function diff_angle which gives the angle between two vectors.
- there is no communication link between satellites
- there is a fixed terrestrial relay on each pole.  

For the  routing of the messages, we propose you to implement a flooding strategy : each time two nodes (satellite<-> antenna or relay) are able to communicate, if one of them has the message, it sends it to the other one. After the communication, the two nodes have the message.

To evaluate the system,  generate at the beginning of the simulation a sender node and a receiver node. Their location is chosen randomly on the Earth. During the simulation, each node which has the message takes a particular color. The simulation stops when the receiver receives the message. To estimate a measure of the duration, print then the number of executions of the updating loop.
   
For the implementation, create a class Message having the following variables :
- received : a boolean which indicates whether the message has been received
- text : the text of the message. Equal to "" if the message is not yet received
- nbHops : the number of hops used to receive the message.

b) Generalize your system in order to have a better estimation of the transmission delay according to the number of orbits. For that, generate a large number of senders  and receivers and measure the various transmission delays. Plot in matplotlib the minimum, the maximum and the average delays according to the number of orbits.  

# DEBUG
in anaconda prompt shell:
```
pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org vpython 
```
